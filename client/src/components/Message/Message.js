import React, { useState } from 'react';
import { Icon, Feed, Button } from 'semantic-ui-react';
import { Mutation } from 'react-apollo';
import Reply from '../Reply/Reply';
import ReplyInput from '../Reply/ReplyInput';
import { LIKE_MUTATION, DISLIKE_MUTATION } from '../../queries';

const Message = ({ message }) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const openModal = isModalOpen => { setIsModalOpen(isModalOpen) };

    const { body, likes, dislikes } = message;
    const id = message.id.substr(-3);
    const messageId = message.id;
    return (
        <React.Fragment>
            {
                isModalOpen && 
                <ReplyInput 
                isModalOpen={isModalOpen}
                openModal={openModal}
                messageId={messageId} 
                />
            }    
            <Feed>
                <Feed.Event>
                    <Feed.Content>
                        <Feed.Extra text className='messageBody'>
                        { body }
                        <div>
                            <div>#{id}</div>
                            <Button size='mini' onClick={ev => setIsModalOpen(true)}>Reply</Button>
                        </div>
                        </Feed.Extra>
                        <Feed.Meta className='reactionDiv'>
                            <Feed.Like>
                                <Mutation
                                    mutation={LIKE_MUTATION}
                                    variables={{ messageId }}
                                >
                                    { postMutation =>  
                                    <Icon 
                                        name='thumbs up outline'
                                        onClick={postMutation}
                                    /> }
                                </Mutation>
                                {likes}
                            </Feed.Like>
                            <Feed.Like>
                                <Mutation
                                    mutation={DISLIKE_MUTATION}
                                    variables={{ messageId }}
                                >
                                    { postMutation =>  
                                    <Icon 
                                        name='thumbs down outline'
                                        onClick={postMutation}
                                    /> }
                                </Mutation>
                                {dislikes}
                            </Feed.Like>
                        </Feed.Meta>
                    </Feed.Content>
                </Feed.Event>
            </Feed>
            <div className='repliesDiv'>
                {message.replies &&
                    message.replies.map(reply => (
                    <Reply key={reply.id} 
                    reply={reply}  
                />
                ))}
            </div>
        </React.Fragment>       
    );
};

export default Message;