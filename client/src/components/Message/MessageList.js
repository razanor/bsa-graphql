import React from 'react';
import { Segment } from 'semantic-ui-react';
import { Query } from 'react-apollo';
import Message from './Message';
import MessageInput from './MessageInput';
import { MESSAGES_QUERY, NEW_MESSAGE_SUBSCRIPTION, NEW_MESSAGE_REACTION_SUBSCRIPTION } from '../../queries';

const MessageList = () => {

    const _subscribeToNewMessage = subscribeToMore => {
    subscribeToMore({
        document: NEW_MESSAGE_SUBSCRIPTION,
        updateQuery: (prev, { subscriptionData }) => {
            if (!subscriptionData.data) return prev;
                const { newMessage } = subscriptionData.data;
                const exists = prev.messages.messageList.find(({ id }) => id === newMessage.id);
                if (exists) return prev;

                return {...prev, messages: {
                messageList: [...prev.messages.messageList, newMessage],
                count: prev.messages.messageList.length + 1,
                __typename: prev.messages.__typename
                }};
            }
        });
    };

    const _subscribeToNewMessageReaction = subscribeToMore => {
        subscribeToMore({
            document: NEW_MESSAGE_REACTION_SUBSCRIPTION
          });
    };

    return (
        <Query query={MESSAGES_QUERY}>
            {({ loading, error, data, subscribeToMore }) => {
                if (loading) return <div>Loading...</div>
                if (error) return <div>Error</div>

                _subscribeToNewMessage(subscribeToMore);
                _subscribeToNewMessageReaction(subscribeToMore);

                const { messages: { messageList } } = data;

                return (
                    <React.Fragment>
                        <Segment className="messagesWrapper">
                            {messageList.map(message => <Message key={message.id} message={message} />)}
                        </Segment>
                        <MessageInput />
                    </React.Fragment>
                );
            }}
        </Query>
    )
};

export default MessageList;