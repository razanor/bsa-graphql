import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { Form, TextArea, Button, Icon } from 'semantic-ui-react';
import { MESSAGES_QUERY,  POST_MESSAGE_MUTATION } from '../../queries';

const MessageInput = () => {
    const [body, setBody] = useState('');

    const _updateStoreAfterAddingMessage = (store, newMessage) => {
        const orderBy = 'createdAt_DESC';
        const data = store.readQuery({
            query: MESSAGES_QUERY,
            variables: {
                orderBy
            }
        });
        data.messages.messageList.push(newMessage);
        store.writeQuery({
            query: MESSAGES_QUERY,
            data
        });
    };

    return (
        <Mutation 
            mutation={POST_MESSAGE_MUTATION} 
            variables={{ body }}
            update={(store, { data: { postMessage } }) => {
                _updateStoreAfterAddingMessage(store, postMessage);
            }}
            onCompleted={() => setBody('')}
        >
        {postMutation => <Form onSubmit={postMutation}>
                <Form.Group inline>
                    <Form.Field 
                        className="message-input" 
                        control={TextArea}  
                        placeholder='Message'
                        onChange={ev => setBody(ev.target.value)}
                        value={body} 
                    />
                    <Button labelPosition='left' icon color='vk'>Send
                        <Icon name='hand point up outline' />
                    </Button>
                </Form.Group>
            </Form>
        }
        </Mutation>
    );
};

export default MessageInput;