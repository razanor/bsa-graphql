import React from 'react';
import MessageList from './Message/MessageList';

function App() {
  return (
    <div className="wrapper">
      <MessageList />
    </div>
  );
}

export default App;
