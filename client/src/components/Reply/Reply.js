import React from 'react';
import { Feed } from 'semantic-ui-react';

const Reply = ({ reply }) => {
    const id = reply.id.substr(-3);
    return (
        <React.Fragment>    
            <Feed className='reply'>
                <Feed.Event>
                    <Feed.Content>
                        <Feed.Extra text className='messageBody'>
                        { reply.body }
                        <div>
                            <div style={{fontSize: '0.9rem'}}>#{id}</div>
                        </div>
                        </Feed.Extra>
                    </Feed.Content>
                </Feed.Event>
            </Feed>
        </React.Fragment>
    )

}

export default Reply;