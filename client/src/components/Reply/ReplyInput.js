import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { Form, TextArea, Button, Modal } from 'semantic-ui-react';
import { MESSAGES_QUERY, POST_REPLY_MUTATION } from '../../queries';

const ReplyInput = ({ openModal, messageId, isModalOpen }) => {
    const [body, setBody] = useState('');

    const _updateStoreAfterAddingReply = (store, newReply, messageId) => {
        const orderBy = 'createdAt_DESC';
        const data = store.readQuery({
            query: MESSAGES_QUERY,
            variables: {
                orderBy
            }
        });
        const repliedMessage = data.messages.messageList.find(
            message => message.id === messageId
        );
        repliedMessage.replies.push(newReply);
        store.writeQuery({
            query: MESSAGES_QUERY,
            data
        });
    };

        return (
            <Modal 
                closeIcon 
                dimmer="blurring" 
                centered={false} 
                open={isModalOpen} 
                onClose={() => openModal(false)}
            >
            <Modal.Header>Add reply</Modal.Header>
                <Modal.Content>
                    <Mutation 
                        mutation={POST_REPLY_MUTATION} 
                        variables={{ messageId, body }}
                        update={(store, { data: { postReply } }) => {
                            _updateStoreAfterAddingReply(store, postReply, messageId);
                        }}
                        onCompleted={() => openModal(false)} 
                    >
                    {postMutation => <Form onSubmit={postMutation}>
                            <Form.Group inline>
                                <Form.Field 
                                className="message-input" 
                                control={TextArea}  
                                placeholder='Reply'
                                onChange={ev => setBody(ev.target.value)}
                                value={body} 
                                />
                                <Button color='blue'>Submit</Button>
                            </Form.Group>
                        </Form>
                    }
                    </Mutation>
                </Modal.Content>
            </Modal>       
    );
}

export default ReplyInput;