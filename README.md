This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm start-server`

Runs the GraphQL server.
Open [http://localhost:4000](http://localhost:4000) to test API in GraphQL Playground.
